__author__ = 'Richard M. transmeta01@gmail.com'

import endpoints
from google.appengine.ext import ndb
from protorpc import remote
from protorpc import messages


class App(messages.Message):
    id = messages.IntegerField(1)
    name = messages.StringField(2, required=True)
    description = messages.StringField(3, required=True)


class AppId(messages.Message):
    id = messages.IntegerField(1, required=True)


class AppModel(ndb.Model):
    name = ndb.StringProperty(required=True)
    description = ndb.StringProperty(required=True)
    promo_count = ndb.IntegerProperty()
    last_modified = ndb.DateTimeProperty(auto_now_add=True)


@endpoints.api(name='promo_exchange', version='v1',
               description='promotion exchange API')
class PromoExchangeAPI(remote.Service):

    @endpoints.method(App, App, path='app',
                      http_method='POST', name='add_app')
    def add_app(self, request):
        # validate that the app does not already exist
        existing_app = AppModel.query(ndb.AND(AppModel.name == request.name,
                                              AppModel.description == request.description)).get()

        if existing_app is None:
            app_key = AppModel(name=request.name, description=request.description, promo_count=0).put()
            return App(name=request.name, description=request.description, id=app_key.id())
        else:
            raise endpoints.BadRequestException('The app already exists')

    """
    function to allow the promotion of an app
    the algorithm for selection of which app is to be promoted is simply a round robin.
    The promotion algorithm rewards the highest promo_count first
    """
    @endpoints.method(AppId, App, path='promote',
                      http_method='GET', name='promote')
    def get_app_to_promote(self, request):
        if request.id is None:
            raise endpoints.BadRequestException('The calling appId is missing in the request')

        # sort to bypass the limitations of multiple inequality filters
        app_list = AppModel.query(AppModel.key != ndb.Key(AppModel, request.id))
        promoted_app = sorted(app_list, key=lambda app: app.promo_count, reverse=True)[0]

        # retrieve the requesting app and update its promo_count
        requesting_app = AppModel.get_by_id(request.id)
        requesting_app.promo_count += 1
        requesting_app.put()

        promoted_app.promo_count -= 1

        promoted_app_key = promoted_app.put()

        return App(id=promoted_app_key.id(), name=promoted_app.name, description=promoted_app.description)


application = endpoints.api_server([PromoExchangeAPI])